#include <stdio.h>
#include <stdlib.h>
#define STACK_SIZE 5
void swap(int *, int *);
int is_empty(void);
int is_full(void);
void print(void);
int pop(void);
void stack_overflow(void);
void clear(void);
void init(void);
void push(int i);
int stack[STACK_SIZE];
int *top_ptr;
int main(void)
{
    init();
    push(1);
    push(2);
    push(3);
    push(56);
    print();
    clear();
    return 0;
}
/////////////STACK/////////////
void stack_overflow(void)
{
    printf("ERROR: Stack overflow\n");
    exit(EXIT_FAILURE);
}
int is_empty()
{
    return top_ptr == &stack[0];
}
void stack_underflow(void)
{
    printf("ERROR: Stack underflow\n");
    exit(EXIT_FAILURE);
}
int is_full(void)
{
    return top_ptr == &stack[STACK_SIZE];
}
void push(int i)
{
    if (is_full())
        stack_overflow();
    printf("Pushing at address 0x%p value %d\n", top_ptr, i);
    *top_ptr++ = i;
}
int pop(void)
{
    if (is_empty())
        stack_underflow();
    return *--top_ptr;
}
void print(void)
{
    int *temp_ptr = &stack[STACK_SIZE];
    while (--temp_ptr >= stack)
        printf("Printing element %d with memory address 0x%p\n", *temp_ptr, temp_ptr);
}
unsigned int size(void)
{
    return STACK_SIZE;
}
void clear(void)
{
    int *temp_ptr = stack;
    while (temp_ptr < stack + STACK_SIZE)
        *temp_ptr++ = 0;
}
void init(void)
{
    top_ptr = &stack[0];
    while (top_ptr < stack + STACK_SIZE)
        *top_ptr++ = 0;
    top_ptr = &stack[0];
}
/////////////STACK/////////////
void swap(int *p, int *q)
{
    int t = *p;
    *p = *q;
    *q = t;
}
